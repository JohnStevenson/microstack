<?php

/*
 * This file is part of the Microstack framework.
 *
 * (c) John Stevenson <john-stevenson@blueyonder.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Microstack;

abstract class Stack extends \Pimple
{
    /**
    * The registered class aliases for lazy loading.
    *
    * @var array
    */
    protected $aliases = array();

    /**
    * The registered middlewares.
    *
    * @var array
    */
    protected $middleware = array();

    /**
    * The registered static classes to proxy.
    *
    * @var array
    */
    protected $staticals = array();

    /**
    * The generic storage object
    *
    * @var \ArrayObject
    */
    protected $storage;

    /**
    * The index of the next middleware to call.
    *
    * @var integer
    */
    private $callIndex = -1;

    /**
     * Instantiate the container, core and statical interface.
     *
     * Parameters and objects can be passed as argument to the constructor.
     *
     * @param array $values The parameters or objects.
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        spl_autoload_register(array($this, 'ClassAliasLoader'));
        Statical::setResolver(array($this, 'resolveStatical'));
        $this->storage = new \ArrayObject();
    }

    /**
    * Registered class loader to manage lazy class aliasing.
    *
    * @param string $class
    */
    public function classAliasLoader($class)
    {
        if (isset($this->aliases[$class])) {
            class_alias($this->aliases[$class], $class);
            unset($this->aliases[$class]);
        }
    }

    /**
    * Resolve __callStatic calls from the static base class.
    *
    * @param string $class
    * @param string $method
    * @param array $args
    * @return mixed
    */
    public function resolveStatical($class, $method, $args)
    {
        if (!isset($this->staticals[$class])) {
            throw new \RuntimeException(
                $class.' not registered as a statical.');
        }

        $instance = $this->staticals[$class][1];
        if (!is_object($instance)) {
            $id = $this->staticals[$class][0];
            $instance = isset($this['service:'.$id]) ?
                $this['service:'.$id] : $this[$id];
            $this->staticals[$class][1] = $instance;
        }

        return call_user_func_array(array($instance, $method), $args);
    }

    /**
    * Call the next middleware.
    *
    * @return void
    */
    public function callNext()
    {
        if (isset($this->middleware[$this->callIndex])) {
            $this->middleware[$this->callIndex++]->call($this);
        }
    }

    public function deferClassAlias($original, $alias)
    {
        $original = ltrim($original, '\\');
        $alias = ltrim($alias, '\\');
        $this->aliases[$alias] = $original;
    }

    public function data()
    {
        return $this->storage;
    }

    public function param($id, $value = null)
    {
        if (null === $value) {
            return $this['param:'.$id];
        }

        if (method_exists($value, '__invoke')) {
            throw new \InvalidArgumentException(
                'Data definition cannot be a Closure or invokable object.');
        }

        $this['param:'.$id] = $value;
    }

    public function service($id, $callable = null)
    {
        if (null === $callable) {
            return $this['service:'.$id];
        }

        if (!method_exists($callable, '__invoke')) {
            throw new \InvalidArgumentException(
                'Service definition is not a Closure or invokable object.');
        }

        $this['service:'.$id] = $callable;
    }

    public function serviceRaw($id)
    {
        return $this->raw('service:'.$id);
    }

    public function serviceFactory($id, $callable)
    {
        $this['service:'.$id] = $this->factory($callable);
    }

    public function serviceExtend($id, $callable)
    {
        $this['service:'.$id] = $this->extend($id, $callable);
    }

    public function serviceProtect($id, $callable)
    {
        $this['service:'.$id] = $this->protect($callable);
    }

    public function serviceStatical($id, $staticClass, $alias, $callable)
    {
        $this->service($id, $callable);
        $this->registerStatical($id, $staticClass, $alias);
    }

    public function registerStatical($accessor, $staticClass, $alias = null)
    {
        $id = is_string($accessor) ? $accessor : null;
        $instance = is_object($accessor) ? $accessor : null;

        if (!$id && !$instance) {
            throw new \InvalidArgumentException(
                'Accessor must be either a string id or a class instance.');
        }

        $staticClass = ltrim($staticClass, '\\');
        $alias = ltrim($alias, '\\');

        if ($instance) {
            if (isset($this->staticals[$staticClass])) {
                throw new \RuntimeException(
                    $staticClass.' already registered as statical.');
            }
            $subject = $alias ?: $staticClass;
            $id = basename(str_replace('\\', '/', $subject));
        }

        $alias = $alias ?: ucfirst($id);
        $this->staticals[$staticClass] = [$id, $instance];
        $this->deferClassAlias($staticClass, $alias);
    }

    protected function addMiddleware($middleware)
    {
        if (!method_exists($middleware, 'call')) {
            throw new \InvalidArgumentException(
                'Middleware class must have a call method.');
        }

        if (in_array($middleware, $this->middleware)) {
            throw new \RuntimeException(
                get_class($middleware).' already added as middleware.');
        }

        $this->middleware[] = $middleware;
    }

    protected function runStack()
    {
        $this->callIndex = 0;
        $this->callNext();
    }
}
