<?php

/*
 * This file is part of the Microstack framework.
 *
 * (c) John Stevenson <john-stevenson@blueyonder.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Microstack;

abstract class Statical
{
    /**
    * The callable used to resolve staticals.
    *
    * @var callable
    */
    protected static $resolver;

    /**
    * Sets the statical resolver for child classes.
    *
    * @param callable $resolver
    * @return void
    */
    public static function setResolver($resolver)
    {
        static::$resolver = $resolver;
    }

    /**
    * Built-in magic method.
    *
    * @param string $method
    * @param array $arguments
    * @return mixed
    */
    public static function __callStatic($method, $arguments)
    {
        $args = [get_called_class(), $method, $arguments];
        return call_user_func_array(static::$resolver, $args);
    }
}
