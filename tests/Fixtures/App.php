<?php
namespace Microstack\Tests\Fixtures;

class App extends \Microstack\Stack
{
    use Helper;

    public function run()
    {
        $this->param('calls', array());
        $this->addMiddleware(new Middleware1);
        $this->addMiddleware(new Middleware2);
        $this->addMiddleware($this);
        $this->runStack();
    }
}

class Middleware1
{
    use Helper;
}

class Middleware2
{
    use Helper;
}

trait Helper
{
    protected $app;

    public function call($app)
    {
        $this->app = $app;

        $this->log('Before');
        $app->callNext();
        $this->log('After');
    }

    private function log($msg)
    {
        $class = basename(str_replace('\\', '/', get_called_class()));
        $calls = array_merge(
            $this->app->param('calls'),
            [$msg.' '.$class]
        );
        $this->app->param('calls', $calls);
    }
}
