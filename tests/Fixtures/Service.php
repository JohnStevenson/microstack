<?php
namespace Microstack\Tests\Fixtures;

class Service
{
    public function getClass()
    {
        return get_called_class();
    }
}
