<?php

namespace Microstack\Tests;

use \Microstack\Tests\Fixtures\App as App;

class StackBaseTest extends \PHPUnit_Framework_TestCase
{

    public function testMiddlewareSequence()
    {
        $app = new App();
        $app->run();
        $calls = $app->param('calls');

        $this->assertEquals('Before Middleware1', $calls[0]);
        $this->assertEquals('Before Middleware2', $calls[1]);
        $this->assertEquals('Before App', $calls[2]);
        $this->assertEquals('After App', $calls[3]);
        $this->assertEquals('After Middleware2', $calls[4]);
        $this->assertEquals('After Middleware1', $calls[5]);
    }

}

