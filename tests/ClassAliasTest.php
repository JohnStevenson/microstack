<?php
namespace Microstack\Tests;

use \Microstack\Tests\Fixtures\App as App;

/**
 * @runTestsInSeparateProcesses
 */
class ClassAliasTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        require 'bootstrap.php';
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testDeferClassAlias()
    {
        $app = new App();

        $original = 'Microstack\\Tests\\Fixtures\\Service';
        $alias = 'Alias';

        $app->deferClassAlias($original, $alias);

        $this->assertFalse(class_exists($original, false));
        $this->assertFalse(class_exists($alias, false));

        $instance = new $alias;
        $this->assertEquals($original, $instance->getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testDeferClassAliasWithSlash()
    {
        $app = new App();

        $original = '\\Microstack\\Tests\\Fixtures\\Service';
        $alias = '\\Alias';

        $app->deferClassAlias($original, $alias);

        $this->assertFalse(class_exists($original, false));
        $this->assertFalse(class_exists($alias, false));

        $instance = new $alias;
        $this->assertEquals(ltrim($original, '\\'), $instance->getClass());
    }

}

