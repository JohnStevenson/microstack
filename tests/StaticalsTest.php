<?php
namespace Microstack\Tests;

use \Microstack\Tests\Fixtures\App as App;

/**
 * @runTestsInSeparateProcesses
 * @backupGlobals disabled
 */
class StaticalsTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        require 'bootstrap.php';
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalNoAlias()
    {
        $app = new App();

        $instance = new \Microstack\Tests\Fixtures\Service();
        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = null;
        $expected = get_class($instance);

        $app->registerStatical($instance, $static, $alias);
        $this->assertEquals($expected, \Service::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalWithAlias()
    {
        $app = new App();

        $instance = new \Microstack\Tests\Fixtures\Service();
        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = 'MyService';
        $expected = get_class($instance);

        $app->registerStatical($instance, $static, $alias);
        $this->assertEquals($expected, \MyService::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalWithAliasAndSlash()
    {
        $app = new App();

        $instance = new \Microstack\Tests\Fixtures\Service();
        $static = '\\Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = '\\MyService';
        $expected = get_class($instance);

        $app->registerStatical($instance, $static, $alias);
        $this->assertEquals($expected, \MyService::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalWithAliasNs()
    {
        $app = new App();

        $instance = new \Microstack\Tests\Fixtures\Service();
        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = 'Ns\\MyService';
        $expected = get_class($instance);

        $app->registerStatical($instance, $static, $alias);
        $this->assertEquals($expected, \Ns\MyService::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalWithService()
    {
        $app = new App();

        $service = 'service';
        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = null;
        $expected = 'Microstack\Tests\Fixtures\Service';

        $app->service($service, function ($app) {
            return new \Microstack\Tests\Fixtures\Service();
        });

        $app->registerStatical($service, $static, $alias);
        $this->assertEquals($expected, \Service::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalWithoutService()
    {
        $app = new App();

        $service = 'service';
        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = null;
        $expected = 'Microstack\Tests\Fixtures\Service';

        $app->registerStatical($service, $static, $alias);

        $app->service($service, function ($app) {
            return new \Microstack\Tests\Fixtures\Service();
        });

        $this->assertEquals($expected, \Service::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testRegisterStaticalWithSelf()
    {
        $app = new App();

        $instance = $app;
        $static = 'Microstack\\Tests\\Fixtures\\Statical\\App';
        $alias = null;

        $app->registerStatical($instance, $static, $alias);

        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $expected = 'Microstack\Tests\Fixtures\Service';

        \App::serviceStatical('service', $static, $alias, function ($app) {
            return new \Microstack\Tests\Fixtures\Service();
        });

        $this->assertEquals($expected, \Service::getClass());
    }

    /**
     * @preserveGlobalState disabled
     * @expectedException   InvalidArgumentException
     */
    public function testRegisterStaticalWithInvalidAccessor()
    {
        $app = new App();

        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = null;

        $app->registerStatical(null, $static, $alias);
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testSetServiceStaticalNoAlias()
    {
        $app = new App();

        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = null;
        $expected = 'Microstack\Tests\Fixtures\Service';

        $app->serviceStatical('service', $static, $alias, function ($app) {
            return new \Microstack\Tests\Fixtures\Service();
        });

        $this->assertEquals($expected, \Service::getClass());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testSetServiceStaticalWithAlias()
    {
        $app = new App();

        $static = 'Microstack\\Tests\\Fixtures\\Statical\\Service';
        $alias = 'MyService';
        $expected = 'Microstack\Tests\Fixtures\Service';

        $app->serviceStatical('service', $static, $alias, function ($app) {
            return new \Microstack\Tests\Fixtures\Service();
        });

        $this->assertEquals($expected, \MyService::getClass());
    }
}

